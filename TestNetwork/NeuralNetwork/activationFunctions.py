import numpy as np

class ActivationFunctions(object):
    def __init__(self):
        return

    @staticmethod
    def sigmoid(x):
        return 1/(1+np.exp(-x))

    @staticmethod
    def sigmoid_prime(x):
        return np.exp(-x)/((1+np.exp(-x))**2)

    @staticmethod
    def sign(x):
        return np.sign(x)

    @staticmethod
    def ternary_sign(x, delta=0.1):
        if (x > delta):
            return 1
        elif (x < -delta):
            return -1
        else:
            return 0

    @staticmethod
    def pentagon_signal(x, delta=0.1, epsilon=0.2):
        if (x > epsilon):
            return 2
        elif (x > delta):
            return 1
        elif (x < -epsilon):
            return -2
        elif (x < -delta):
            return -1
        else:
            return 0
