import matplotlib.pyplot as plt
import numpy as np

def read_log_file(log_file_name, train=False):
    log_file = {
        'top1': list(),
        'top5': list(),
        'error': list()
    }
    with open("{0}.log".format(log_file_name)) as test_file:
        test_file.readline()
        if train:
            for line in test_file:
                values = list(map(float,line.split()))
                log_file['top1'].append(values[2])
                log_file['top5'].append(values[0])
                log_file['error'].append(values[1])
        else:
            for line in test_file:
                values = list(map(float,line.split()))
                log_file['top1'].append(values[0])
                log_file['top5'].append(values[1])
                log_file['error'].append(values[2])
    return log_file

def main():

    test_alexnet = read_log_file('test_alexnet', True)
    print(test_alexnet)
    train_alexnet = read_log_file('train_alexnet', True)
    print(train_alexnet)

    train_binarynet = read_log_file('train_binarynet', True)
    print(train_binarynet)

    train_xnornet = read_log_file('train_xnor-net', True)
    print(train_xnornet)

    train_ternet = read_log_file('train-ternarynet', True)
    print(train_ternet)

    t = np.arange(0, 19, 1)
    s = train_alexnet['top1']

    plt.figure(1)
    plt.grid(True)
    plt.plot(
        np.arange(1, len(train_alexnet['top1'])+1,1), train_alexnet['top1'], '*-b',
        np.arange(1, len(train_binarynet['top1'])+1,1), train_binarynet['top1'], 'x:r',
        np.arange(1, len(train_xnornet['top1'])+1,1), train_xnornet['top1'], 's-.g',
        np.arange(1, len(train_ternet['top1'])+1,1), train_ternet['top1'], 'y--o'
    )
    plt.xlim([1,20])
    plt.xticks(np.arange(1,20,1))
    plt.xlabel('Epochs')
    plt.ylabel('Top-1 (%)')
    plt.title('Top-1 Training Data')
    plt.legend(['alexnet','binarynet','xnor-net (Binary)','xnor-net (Ternary)'])
    plt.show()

    plt.figure(2)
    plt.grid(True)
    plt.plot(
        np.arange(1,len(train_alexnet['top5'])+1,1), train_alexnet['top5'], '*-b',
        np.arange(1,len(train_binarynet['top5'])+1,1), train_binarynet['top5'], 'x:r',
        np.arange(1,len(train_xnornet['top5'])+1,1), train_xnornet['top5'], 's-.g',
        np.arange(1,len(train_ternet['top5'])+1,1), train_ternet['top5'], 'y--o'
    )
    plt.xlim([1,20])
    plt.xticks(np.arange(1,20,1))
    plt.xlabel('Epochs')
    plt.ylabel('Top-5 (%)')
    plt.title('Top-5 Training Data')
    plt.legend(['alexnet','binarynet','xnor-net (Binary)','xnor-net (Ternary)'])
    plt.show()

    plt.figure(3)
    plt.grid(True)
    plt.plot(
        np.arange(1, len(train_alexnet['error'])+1,1), train_alexnet['error'], '*-b',
        np.arange(1, len(train_binarynet['error'])+1,1), train_binarynet['error'], 'x:r',
        np.arange(1, len(train_xnornet['error'])+1,1), train_xnornet['error'], 's-.g',
        np.arange(1, len(train_ternet['error'])+1,1), train_ternet['error'], 'y--o'
    )
    plt.xlim([1,20])
    plt.xticks(np.arange(1,20,1))
    plt.xlabel('Epochs')
    plt.ylabel('Error')
    plt.title('Training Error')
    plt.legend(['alexnet','binarynet','xnor-net (Binary)','xnor-net (Ternary)'])
    plt.show()

    ################################################################


    test_alexnet = read_log_file('test_alexnet')
    print(test_alexnet)

    test_binarynet = read_log_file('test_binarynet')
    print(test_binarynet)

    test_xnornet = read_log_file('test_xnor-net')
    print(test_xnornet)

    test_ternet = read_log_file('test-ternarynet')
    print(train_ternet)

    t = np.arange(0, 19, 1)
    s = train_alexnet['top1']

    plt.figure(1)
    plt.grid(True)
    plt.plot(
        np.arange(1,len(test_alexnet['top1'])+1,1), test_alexnet['top1'], '*-b',
        np.arange(1,len(test_binarynet['top1'])+1,1), test_binarynet['top1'], 'x:r',
        np.arange(1,len(test_xnornet['top1'])+1,1), test_xnornet['top1'], 's-.g',
        np.arange(1,len(test_ternet['top1'])+1,1), test_ternet['top1'], 'y--o'
    )
    plt.xlim([1,20])
    plt.xticks(np.arange(1,20,1))
    plt.xlabel('Epochs')
    plt.ylabel('Top-1 (%)')
    plt.title('Top-1 Testing Data')
    plt.legend(['alexnet','binarynet','xnor-net (Binary)','xnor-net (Ternary)'])
    plt.show()

    plt.figure(2)
    plt.grid(True)
    plt.plot(
        np.arange(1,len(test_alexnet['top5'])+1,1), test_alexnet['top5'], '*-b',
        np.arange(1,len(test_binarynet['top5'])+1,1), test_binarynet['top5'], 'x:r',
        np.arange(1,len(test_xnornet['top5'])+1,1), test_xnornet['top5'], 's-.g',
        np.arange(1,len(test_ternet['top5'])+1,1), test_ternet['top5'], 'y--o'
    )
    plt.xlim([1,20])
    plt.xticks(np.arange(1,20,1))
    plt.xlabel('Epochs')
    plt.ylabel('Top-5 (%)')
    plt.title('Top-5 Testing Data')
    plt.legend(['alexnet','binarynet','xnor-net (Binary)','xnor-net (Ternary)'])
    plt.show()

    plt.figure(3)
    plt.grid(True)
    plt.plot(
        np.arange(1,len(test_alexnet['error'])+1,1), test_alexnet['error'], '*-b',
        np.arange(1,len(test_binarynet['error'])+1,1), test_binarynet['error'], 'x:r',
        np.arange(1,len(test_xnornet['error'])+1,1), test_xnornet['error'], 's-.g',
        np.arange(1,len(test_ternet['error'])+1,1), test_ternet['error'], 'y--o'
    )
    plt.xlim([1,20])
    plt.xticks(np.arange(1,20,1))
    plt.xlabel('Epochs')
    plt.ylabel('Error')
    plt.title('Testing Error')
    plt.legend(['alexnet','binarynet','xnor-net (Binary)','xnor-net (Ternary)'])
    plt.show()

if __name__ == '__main__':
    main()
