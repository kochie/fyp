Requirements Analysis
======

Robert Koch - 25124390
------

[[TOC]]
++++

# Introduction

## Context
As computational power has increased exponentially over the last half century so to has the number of applications that have been feasible to tackle with computers. However certain classes of problems such as image classification, speech recognition, and several problems in decision making have proven difficult to solve in reasonable times with conventional algorithms and modern CPUs, machine learning using methodologies like neural networking provide a possible solution to this obstacle.

The goal of machine learning is to give computers the ability to learn without being explicitly programmed, this opens up an entire new class of problems that have been difficult for computers to solve but easy for humans. Many computer vison problems are in this class, as well as data filtering problems like spam detection.

Machine learning, while not an entirely new concept has made considerable advancements in the last decade thanks to new algorithms using faster CPUs and parallel architectures like GPUs. One such innovation has come from the resurgence of neural networks, a computational model analogous to the observed behavior of biological brains. The model uses a collection of neural units that are connected with several others in either a sparsely or dense configuration, and such connections can either enhance or inhibit the activation state of adjacent neural units. Each neural unit has a weight attributed to it which can be considered the score that unit contributes to the output, this weight can be changed by training the network and using algorithms like gradient descent combined with back propagation to obtain a more accurate result.

Recently advancements in the field such as [Alex-Net](http://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf) have increased the accuracy and speed of models attempting the image recognition problem. Modifications to this algorithm include changing to binary activation levels and weights as seen in [Binarized Neural Networks](https://arxiv.org/pdf/1602.02830.pdf) and later in [XNOR-Net](https://arxiv.org/pdf/1603.05279.pdf) where in the latter using XNOR bitwise operations resulted in convolutional operations increasing by a factor of 58 while memory use decreased by a factor of 32 while still being within 3% of the original Alex-Net value.

## Objectives

There are several objectives for this project that mostly surround exploring other potential use cases for such networks and if possible optimize for such purposes.

- Investigate the precision of XNOR-Net against the speed of the algorithm and see what changing certain variables such as activations, training times, and size does to the speed and accuracy of the classifications.

- Modify the back propagation method that currently bit shifts the neuron weights.

- Increase the number of channels in the model and observe the effects on the output accuracy and speed.

- Increase the number of weight levels to include -1, 0, 1 and view the effects on speed and accuracy and see if having the 0 level increases both speed and accuracy since the extra level should improve accuracy and the zero level should require no arithmetic to compute.

- (Stretch) Find the lowest level of hardware required to run a network and investigate if it's possible to execute on an embedded system such as a laptop or phone.

# Requirements

## Project Overview

The project has several goals, which have been split into several phases:

- Initial research into binary neural networks.
- Experiment and investigate changing values of variables and understand their relationship to the operation of the network.

## Requirements

At a high level the project should meet the following requirement:

> Explore binary and XNOR neural networks by looking at new applications where precision and speed can be modified by adding more channels and using a turnery weight system.

## Functional Requirements

The system must have the following functional requirements:

- Project must show the development and operation of the programs that complete each objective. If an objective is unachievable a credible explanation as to why must be documented - possibly with solutions.

- Neural Networks must be tested and trained with several image libraries - including Image-Net 2012 and the comparison between the developed system, Alex-Net, and XNOR-Net must be documented and presented.

## Non-Functional Requirements

- All builds must pass testing and succeed.
- All documentation must be up to date and pass linting.
- All programs must have a redundant installation procedure that is simple to follow and get up and running.
