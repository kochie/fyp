# Design Report
**Robert Koch - 25124390**
[[toc]]
+++
## Introduction
The XNOR-Net neural network is a modification to the alexnet network in which the activation function and weights in the convolution layers are replaced with binary values. This change decreses the accuracy of the network bu significantly increases the training time as most of the mathematical floating point tensor operations can be replaced by bitwise operations. This change allows the network to be implemented on significantly cheaper hardware without the need of intensive GPU operations.

XNOR-Net will be modified with several changed to the network including the use of ternary weights and activation functions. By changing the networks activation functions to ternary levels instead of binary the accuracy of the network can be increased due to the extra level of precision and the speed of the network should remain the same as the third level of 0 requires no extra computation as values will cancel out.

## Specification
The modifications specified for this project include changing the activation functions in the convolutional layers to a ternary level system.

![Activation Functions](./ActivationFunctions.png)

The figure above shows an example of different activation functions. Beginning with standard functions like sigmoid and tanh, the two new ones used in the modified XNOR-Net are the sign and ternary functions which provide simple approximations for the continuous functions. It can be seen in this example that additionally more complicated approximations with more than 3 levels are possible and will be tested hopefully in the coming weeks.

![Top 1 Testing](./Top1Testing.png)

From the above testing data the initial results of the different networks do not match predictions precisely. The binary-weighted version of alexnet far exceeds the other network types. It was expected that the ternary version of XNOR-Net would also be an imporvement over the binary verison, one possible explaination for this is that the results are only for small epochs, normally training can be for as many as 55 epochs.

The ternary code compared to the binary activation function is shown below. This basic definition of 

```lua
function BinActiveZ:updateOutput(input)
	local s = input:size()
   self.output:resizeAs(input):copy(input)
   self.output=self.output:sign();
   return self.output
end
```

```lua
function TriActiveZ:updateOutput(input)
	local s = input:size()
		self.output:resizeAs(input):copy(input)
		local smallOutput = torch.CudaTensor(s):resizeAs(input):copy(input)
		smallOutput[input:ge(0.1)]=1
		smallOutput[input:lt(0.1)]=0
		self.output[input:le(-0.1)]=-1
		self.output[input:gt(-0.1)]=0
		self.output:long():cbitor(smallOutput:long())
		return self.output:cuda()
end
```

The ternary function is a piecewise function as described by the equation below, where $$\delta$$ is the activation value (In this report it has a value around 0.1-2). To create this in Torch first the tensor is cloned into two variables and the two respected values of activation are given as binary operators. After this is done the tensors are then bitwise or together and returned.

$$$
Ternary(x) = 
\left\{
	\begin{array}{ll}
		1 & x \ge \delta\\
		-1 & x \le -\delta\\
		0 & abs(x) < \delta
	\end{array}
\right.
$$$


![Top 5 Testing](./Top5Testing.png)

The Top-5 testing data matches the Top-1 data. The trend of the data 

![Error Testing](./ErrorTesting.png)

## Design Layout
The network as described by XNOR-Net has 6 binary convolution layers. For the modified network these binary layers were converted to ternary layers, and can be seen below.

![](./NetworkLayout.png)
XNOR-Net has 15 layers in total, although half are used for data processing, there are 6 convolutional layers described in the above diagram, these layers all have weights and activation functions that deal in ternary levels.

The models code can also be seen in the appendix.
## Requirements
The next part of the design includes moving the model over from Torch to TensorFlow to increase development speed and testing.

Creating a more robust framework where more values can be tweaked more rapidly.
## Appendix 

XNOR-Net model created in Lua.
```lua
function createModel()
  require 'cudnn'
  local function BinActivation()
    local C= nn.Sequential()
    C:add(nn.BinActiveZ())
    return C
  end

  local function TriActivation()
    local C= nn.Sequential()
    C:add(nn.TriActiveZ())
    return C
  end

  local function MaxPooling(kW, kH, dW, dH, padW, padH)
    return nn.SpatialMaxPooling(kW, kH, dW, dH, padW, padH)
  end

   local function BinConvolution(nInputPlane, nOutputPlane, kW, kH, dW, dH, padW, padH)
         local C= nn.Sequential()
          C:add(nn.SpatialBatchNormalization(nInputPlane,1e-4,false))
          C:add(TriActivation()) -- <---
   		    C:add(cudnn.SpatialConvolution(nInputPlane, nOutputPlane, kW, kH, dW, dH, padW, padH))
   		 return C
   end

    local function BinMaxConvolution(nInputPlane, nOutputPlane, kW, kH, dW, dH, padW, padH,mW,mH)
         local C= nn.Sequential()
          C:add(nn.SpatialBatchNormalization(nInputPlane,1e-4,false))
          C:add(TriActivation()) -- <---
          C:add(cudnn.SpatialConvolution(nInputPlane, nOutputPlane, kW, kH, dW, dH, padW, padH))
          C:add(MaxPooling(3,3,2,2))
       return C
   end

   local features = nn.Sequential()

   features:add(cudnn.SpatialConvolution(3,96,11,11,4,4,2,2))
   features:add(nn.SpatialBatchNormalization(96,1e-5,false))
   features:add(cudnn.ReLU(true))
   features:add(MaxPooling(3,3,2,2))

   features:add(BinMaxConvolution(96,256,5,5,1,1,2,2))
   features:add(BinConvolution(256,384,3,3,1,1,1,1))
   features:add(BinConvolution(384,384,3,3,1,1,1,1))
   features:add(BinMaxConvolution(384,256,3,3,1,1,1,1))
   features:add(BinConvolution(256,4096,6,6))
   features:add(BinConvolution(4096,4096,1,1))

   features:add(nn.SpatialBatchNormalization(4096,1e-3,false))
   features:add(cudnn.ReLU(true))
   features:add(cudnn.SpatialConvolution(4096, nClasses,1,1))

   features:add(nn.z(nClasses))
   features:add(nn.LogSoftMax())

   local model = features
   return model
end
```