# Theory and Background

## Floating Point Numbers
A Floating point number is a representation of a real number that is approximated so that is is possible to store on a computer with a finite number of bits. This trade-off of precision compared to range allows the numbers to be stored efficiently. All programming languages have some method for storing and manipulating floats, with most modern representations of single floats comprising of 32-bits, with 8-bits used for the exponent. Floating point numbers are ideal for storing precise values of real numbers compared to integers because they support decimal places, compared to integer precision arithmetic, floating point is more resource intensive on most systems because it requires more CPU operations - in fact, the number of FLoating Point Operations Per Second(FLOPS) is used as a measurement in supercomputing benchmarks.

## Non Linear Functions
In mathematics a function is a relation between a set of inputs and a set of outputs. Each input is mapped to exactly one output, however multiple inputs can map to the same output. A non-linear function is a relationship where the inputs are not proportional to the outputs, they are of interest to many fields in science and engineering because most systems in nature are inherently non-linear. 

A linear function can be easily defined mathematically as a function which holds to these properties:

By definition a non-linear function is one that does not follow either of the above properties. These functions can be used in a neural network as activation functions so the network can accurately model non-linear systems.

## Mathematical Optimisation
In mathematics, optimisation is the selection of a best value from a specific set, based on some sort of criteria. The input set can be the output of a function, this gives a formal method for optimising a mathematical function to get a specific output. This can also be described as below:


One of the simplest forms of an optimisation problem if that of finding the minimum or maximum outputs of a function, this can be achieved by selecting input values from within a selected range and then comparing the returned value.

## Deep Learning
Deep learning is a subset of machine learning focusing on methods based around learning specific data representations to solve problems rather than task-specific algorithms that are programmed to solve a problem. A deep learning algorithm can learn how to solve a problem without being specifically programmed how, using either a supervised, partially supervised, or unsupervised algorithm. The great advantage that deep learning algorithms have is that they can preform better than people at specific tasks, and this set of tasks is increasing.

### Artificial Neural Networks
An artificial neural network is a computing system inspired by the design of the human brain. In simplistic terms the system is analogous to the operation of neurons and dendrites in an animal brain. In a biological brain, brain cells are connected in a network to preform operations, each brain cell has multiple dendrites that act as connectors which link to other brain cells by their terminals. When enough chemical energy is received by a cell through its dendrites, it fires an signal through its axon which will activate any dendrites connected to its terminal. 

In artificial networks these complicated interactions are replaced with mathematical operations. The basic design of a network is to have input nodes connected to other nodes further in the network like a directed, weighted graph. The vertices in the graph act like brain cells while the edges act as dendrites connecting cells. The edges of the graph are weighted with a random unknown variable, by training the network the values for the weights of each edge can be evaluated to give the least amount of error. Each node in a network sums up its inputs and applies an activation function to the value. This new value becomes the output of the node and is passed on to any connected node.

In more recent times, the analogy to biological brains has diminished and is no longer a predominant guide in the field of deep learning. The main reason for this paradigm shift is simply because we do not know enough about how the human brain works to make any more assumptions about solving problems with this system. For further advancements in copying the human brain it would be necessary to monitor the activity of thousands of interconnected neurons simultaneously.

#### Layout
The layout of networks is a continuing topic of research today, there are hundreds of different designs for specific use cases, but all these designs follow a basic outline. Networks consist of an input layer which is connected to several middle layers, which are connected to the output layer. These middle "hidden" layers, named because their hyperparameters are unknown can have different configurations, ranging from fully-connected to convolution-sparse layers.

#### Weights
The weights in standard neural networks are floating point numbers that can be tuned to lower the output error of the network. When initialised a networks weights are assigned randomly from a distribution. One problem arises with random assignments is that if a weights value is zero initially it can be difficult for it to be retuned, to avoid this a bias factor is added to the distribution to mitigate the issue.

#### Activation Functions
The activation functions of a node defines the output given by a set of inputs. A non-linear activation function allows a network to compute non-trivial solutions to a problem using only a relatively small set of nodes. There are many different activation functions available to use, however most of these functions are similar enough that there is little need for different functions. Using our biological metaphor, the activation functions represents the rate of nerve firing in the brain.

Current investigations into optimising neural networks revolve around converting these non-linear functions into approximate piecewise linear functions that can mimic the output of these functions but at a faster rate with less accuracy. 

#### Backpropagation
Backpropagation is a method used in the training of neural networks to calculate and minimise the error that is propagated by the individual weights found across the network. The algorithm requires some sort of loss function to work, this is usually given by the mean squared error of a batch of outputs. The term backwards propagations comes from the fact that the loss function is calculated on the output node and the error is distributed back through the network using a technique similar to the chain rule in calculus.

Backpropagation requires a known, desired output for each input value, and is therefore known as a supervised learning algorithm.

#### Overfitting
Overfitting is a problem encountered in machine learning and other statistical forms of mathematics where the model being analysed to fit on a dataset becomes too dependant on the data to make future accurate predictions. This is because if the model fits too perfectly to the data small errors in the distribution or other sources of noise can cause the model to become sensitive to false positives.This means the model will likely have a higher error rate on unseen data.

This can become a serious problem for neural networks and is a topic of current research. There are several solutions to overfitting, these include having a large dataset, having less parameters in a model, and input manipulation. 

### Quantising
Quantisation covers a lot of different techniques to store numbers and preform calculations in a more compact and efficient way than typical 32-bit floating point numbers. For the purposes of this report quantisation refers to the simplification of activation functions to piecewise linear functions, and the conversion of floating point weights to integer weights.

## Programming

### CUDA
CUDA is a parallel computing platform based around Nvidia GPU chipsets. It provides a way for developers to create software that can efficiently use GPUs in computers for general purpose processing. The CUDA platform is a software layer that gives developers access to the GPUs internal virtual instruction set, allowing code to be executed in parallel at a much faster rate. CUDA is used for a variety of deep learning solutions as much of the maths needed for deep learning algorithms can be easily computed using the parallel processing power of a graphics card.

### Torch - Lua
Torch is a mathematical framework written in Lua for the specific use of processing scientific data. Torch can compile code to CUDA instructions and execute operations on both the GPU and CPU. Torch has a wide range of functions that can be used when developing deep learning algorithms. The main branch of Torch was written in Lua, a language specifically created to handle scientific data, however it is not a commonly taught language any more, because of this an alternative version of the framework known as PyTorch was written from the ground up in Python to evolve the framework for a new language. 

### TensorFlow - Python
TensorFlow is a framework written in Python to handle the creation and manipulation of Tensors. In mathematics a Tensor is a geometric object that describes a relation between a scalar, vector, matrix, or another Tensor. In fact a Tensor can be thought of as an extension of a matrix in a higher dimension. The purpose of TensorFlow is to create an easy system to manipulate Tensors, this is done in the framework by creating a graph which describes what operations will be preformed on each Tensor and how the data will flow through the graph - hence the name TensorFlow. After the graph has been defined in Python (or any supported high level language for that matter) the graph is then interpreted into a target language, either C or CUDA and run on the targets processing unit. Because of the ease of use, large developer community, and several other features including TensorBoard, many developers and academics have transitioned to using TensorFlow instead of competing frameworks.  

## Optimisation via Binary and Ternary Quantisation
As mentioned above the activations functions of a network are what allow networks to solve problems in a relatively small amount of space and time, however by changing how the activation functions work it is possible to increase the speed, and decrease the training time needed to create a working neural network. In this report we will investigate how changing the activation functions to piecewise linear steps modifies the behaviour and characteristics of several different types of networks. 

### Current Research
Optimisation via quantisation is currently an active field of research in machine learning and there are several efforts currently underway to increase the speed and accuracy of networks using similar techniques that are talked about in this report.

#### Binary-Net
In 2016 a team of researchers in North America and Israel created a network known colloquially known as Binary-Net in this report that constrained the activation function outputs and weights to the values of -1 and 1. Their results showed that is is possible to obtain near state-of-the-art results using a binary network while only using a fraction of memory compared to previous networks that had the same results.

#### XNOR-Net
XNOR-Net is another network that was published in 2016 by researchers from the University of Washington. This network expands the field of quantisation by adapting models like VGG, ResNet, and similar binary models so that most of their internal operations are bitwise functions. By converting specific operations such as convolution, and dot products to approximate binary equivalents XNOR-Net again was able to achieve near-state-of-the-art results equivalent to a full precision network while at the same time reducing the footprint needed to compute results by as much as a factor of 58.

#### Ternary-Net
By expanding on the work of XNOR-Net and Binary-Net researches again improved the accuracy and compression of a network, this time in China. Academics at the Institute of Applied Math in Beijing were able ran tests on network designs that are similar to the ones mentioned in this report. In their research they created a series on models that used piecewise ternary step functions as the activation functions for convolution layers in their reproductions of VGG-19. Their ternary results showed improvements over their binary models, with increases in accuracy by approximately 5%.
