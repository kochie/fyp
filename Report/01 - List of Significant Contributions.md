# List of Significant Contributions

- Created a new library for the development and testing of Neural Networks using the TensorFlow framework, with the ability to:
    - Train a network on any custom database.
    - Run multiple training sessions on multiple GPUs.
    - Create networks comprised of "quantised" components.
    - Automatically create datasets from directory hierarchies.
    - Train networks until a specific error rate has been achieved.
    - Display useful information through the TensorBoard product.
- Decreased the training time of image classification networks by implementing binary and ternary quantised states in the network.
- Improved the accuracy of binary networks by incorporating a third activation level in the network.
- Compared the speed, accuracy, and resource cost of several networks including AlexNet, VGG, and Inception to binary, and ternary counterparts.
- Compared the speed, accuracy, and resource cost these networks had on different datasets to determine any differences in training.
- Compared the filesizes of several optimised and non optimised networks.
- Compared the speed of several types of networks both optimised and not optimised in regards to input batch sizes.
- Modified an existing Torch repository to include optimised models of several networks using Binary and Ternary activation functions.

