\contentsline {section}{List of Significant Contributions}{2}
\contentsline {section}{Poster}{2}
\contentsline {section}{Executive Summary}{4}
\contentsline {section}{List of Figures}{6}
\contentsline {section}{\numberline {1}Introduction}{7}
\contentsline {section}{\numberline {2}Theory and Background}{8}
\contentsline {subsection}{\numberline {2.1}Floating Point Numbers}{8}
\contentsline {subsection}{\numberline {2.2}Non Linear Functions}{8}
\contentsline {subsection}{\numberline {2.3}Mathematical Optimisation}{8}
\contentsline {subsection}{\numberline {2.4}Deep Learning}{8}
\contentsline {subsubsection}{\numberline {2.4.1}Artificial Neural Networks}{9}
\contentsline {paragraph}{Layout}{9}
\contentsline {paragraph}{Weights}{9}
\contentsline {paragraph}{Activation Functions}{10}
\contentsline {subsubsection}{\numberline {2.4.2}Backpropagation}{10}
\contentsline {subsubsection}{\numberline {2.4.3}Overfitting}{11}
\contentsline {subsubsection}{\numberline {2.4.4}Quantising}{11}
\contentsline {subsection}{\numberline {2.5}Programming}{12}
\contentsline {subsubsection}{\numberline {2.5.1}CUDA}{12}
\contentsline {subsubsection}{\numberline {2.5.2}Torch - Lua}{12}
\contentsline {subsubsection}{\numberline {2.5.3}TensorFlow - Python}{13}
\contentsline {subsection}{\numberline {2.6}Optimisation via Binary and Ternary Quantisation}{13}
\contentsline {subsubsection}{\numberline {2.6.1}Current Research}{13}
\contentsline {paragraph}{Binary-Net}{13}
\contentsline {paragraph}{XNOR-Net}{13}
\contentsline {paragraph}{Ternary-Net}{13}
\contentsline {section}{\numberline {3}Torch Implementation}{14}
\contentsline {subsection}{\numberline {3.1}Outline}{14}
\contentsline {subsection}{\numberline {3.2}Development and Programming}{14}
\contentsline {subsection}{\numberline {3.3}Challenges and Solutions}{14}
\contentsline {section}{\numberline {4}TensorFlow Implementation}{15}
\contentsline {subsection}{\numberline {4.1}Outline}{15}
\contentsline {subsection}{\numberline {4.2}Research and Results}{16}
\contentsline {subsection}{\numberline {4.3}Improvements and Adaptations}{16}
\contentsline {section}{\numberline {5}Results and Analysis}{16}
\contentsline {subsection}{\numberline {5.1}Network Error Rates}{16}
\contentsline {subsection}{\numberline {5.2}Network Training and Testing Accuracies}{18}
\contentsline {subsubsection}{\numberline {5.2.1}Top-1 Accuracies}{18}
\contentsline {subsubsection}{\numberline {5.2.2}Top-5 Accuracies}{20}
\contentsline {subsection}{\numberline {5.3}Batch and Filesize Comparisons}{22}
\contentsline {subsection}{\numberline {5.4}Analysis}{23}
\contentsline {section}{\numberline {6}Conclusion}{24}
\contentsline {section}{References}{25}
