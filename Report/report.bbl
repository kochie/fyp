\begin{thebibliography}{10}

\bibitem{AlexNetCar}
Katherine Bourzac.
\newblock
  \url{https://spectrum.ieee.org/computing/embedded-systems/bringing-big-neural-networks-to-selfdriving-cars-smartphones-and-drones},
  2017.

\bibitem{BinaryNet}
Matthieu Courbariaux and Yoshua Bengio.
\newblock Binarynet: Training deep neural networks with weights and activations
  constrained to +1 or -1.
\newblock {\em CoRR}, abs/1602.02830, 2016.

\bibitem{10.1057/9780230226203.0329}
W.~Erwin Diewert.
\newblock cost functions.
\newblock In Steven~N. Durlauf and Lawrence~E. Blume, editors, {\em The New
  Palgrave Dictionary of Economics}. Palgrave Macmillan, Basingstoke, 2008.

\bibitem{DeepLearning}
Ian Goodfellow, Yoshua Bengio, and Aaron Courville.
\newblock {\em Deep Learning}.
\newblock MIT Press, 2016.
\newblock \url{http://www.deeplearningbook.org}.

\bibitem{HandsOn}
Aurélien Géron.
\newblock {\em Hands-On Machine Learning with Scikit-Learn and Tensorflow}.
\newblock O'Reilly Media Inc., 2017.

\bibitem{hinrichsen2005mathematical}
Diederich Hinrichsen.
\newblock {\em Mathematical systems theory I : modelling, state space analysis,
  stability and robustness}.
\newblock Springer, Berlin New York, 2005.

\bibitem{AlexNet}
Alex Krizhevsky, Ilya Sutskever, and Geoffrey~E Hinton.
\newblock Imagenet classification with deep convolutional neural networks.
\newblock In F.~Pereira, C.~J.~C. Burges, L.~Bottou, and K.~Q. Weinberger,
  editors, {\em Advances in Neural Information Processing Systems 25}, pages
  1097--1105. Curran Associates, Inc., 2012.

\bibitem{MarsCopter}
Elizabeth Landau.
\newblock \url{https://www.jpl.nasa.gov/news/news.php?feature=4457}, 2017.

\bibitem{backprop}
Yann Lecun, L~Bottou, Genevieve Orr, and Klaus-Robert Müller.
\newblock Efficient backprop.
\newblock {\em Research Gate}, 1524, 07 1998.

\bibitem{TernaryWeightNetworks}
Fengfu Li and Bin Liu.
\newblock Ternary weight networks.
\newblock {\em CoRR}, abs/1605.04711, 2016.

\bibitem{TensorFlowAndroid}
John Mannes.
\newblock Google's tensorflow lite brings machine learning to android devices.
\newblock
  \url{https://techcrunch.com/2017/05/17/googles-tensorflow-lite-brings-machine-learning-to-android-devices/}.

\bibitem{IntelChip}
Dr.~Michael Mayberry.
\newblock
  \url{https://newsroom.intel.com/editorials/intels-new-self-learning-chip-promises-accelerate-artificial-intelligence/},
  2017.

\bibitem{ConnectionismHistory}
David~A. Medler.
\newblock A brief history of connectionism.
\newblock {\em Neural Computing Surveys}, 1, 1998.

\bibitem{XNORNet}
Mohammad Rastegari, Vicente Ordonez, Joseph Redmon, and Ali Farhadi.
\newblock Xnor-net: Imagenet classification using binary convolutional neural
  networks.
\newblock {\em CoRR}, abs/1603.05279, 2016.

\bibitem{GeneticNN}
A.~J. F.~van Rooij.
\newblock {\em Neural network training using genetic algorithms}.
\newblock World Scientific, 1998.

\bibitem{StochasticNN}
Claudio Turchetti.
\newblock {\em Stochastic models of neural networks}.
\newblock IOS Press,, 2004.

\end{thebibliography}
