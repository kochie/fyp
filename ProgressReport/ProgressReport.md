# Progress Report
**Robert Koch - 25124390**

[[toc]]
+++
## Objectives
Several objectives were outlined in the requirements analysis, since then numerous modification have been made to the project and the resources used. Originally the primary scope of this project was to investigate the precision of the XNOR-Net network in comparison with several other popular neural network configurations including alexnet and its binary weighted equivalent. Going further using this research as a foundation the next objective is to modify XNOR-Net to increase its accuracy and speed by modifying particular characteristics of the network including weight levels, channel number, and activation functions.

Depending on the progress made surrounding the primary scope of this project further research into more advanced applications of such a network can be carried out. Creating a network class that can be applied to a variety of classification and regression problems.

Originally the network used in researching was created in Lua's Torch framework, an older and more academic focused framework. This initial decision caused some difficulty which will be detailed in further sections along with an updated approch to the primary objectives.
 
## Progress To Date
As of writing this report the main objective of modifying XNOR-Net to use ternary weights and activation functions has been achieved, however there is still much to accomplish and several interesting behavious have been observed.

### Timeline
The first several weeks were focused on research and testing of previously created networks. Installing and learning the Torch framework also occured in this time. During the mid semester break a modified version of the binary weight XNOR-Net was converted to use a ternary activation function in the convolution layers. During the last few weeks the main focus has been on gathering more test runs on the modified network and measuring the results compared to other network architectures both on University computers and my Home computer which has similar specs to the machine at University.

### Results
The following plots show the Top-1 accuracy of all network types that were compared in both testing and training scenarios. An interesting observation was that alexnet with binary weights out-preforms all other network designs including the original alexnet. This was an unexpected result that occured over several training sessions on different machines. It was expected that binary weights would increase the speed of the training sessions however this would be at the expense of the models accuracy as binary weights and activations cause a large amount of information in the spatial convolution layer - which reads the image data, to be lost in the convolution layers.

![Top 1 Training](./Top1Training.png)

![Top 1 Testing](./Top1Testing.png)

Another issue that arose during training of the networks was the increase in memory used by torch during each epoch, causing slowdowns, and crashes due to memory leaks. Several solutions were explored including increasing the amount of virtual memory and reconfiguring and debugging the garbage collector, however no solution was found for the unexpected use of memory. 

### Modelling Changes
The change from binary activations to ternary activations 

```lua
function TriActiveZ:updateOutput(input)
	local s = input:size()
		self.output:resizeAs(input):copy(input)
		local smallOutput = torch.CudaTensor(s):resizeAs(input):copy(input)
		smallOutput[input:ge(0.1)]=1
		smallOutput[input:lt(0.1)]=0
		self.output[input:le(-0.1)]=-1
		self.output[input:gt(-0.1)]=0
		self.output:long():cbitor(smallOutput:long())
		return self.output:cuda()
end

function TriActiveZ:updateGradInput(input, gradOutput)
   local s = input:size()
   self.gradInput:resizeAs(gradOutput):copy(gradOutput)
   self.gradInput[input:ge(1)]=0
   self.gradInput[input:le(-1)]=0
   return self.gradInput
end
```

The above code is an exscript from the network architecture showing how the ternary activation functions is implemented using CUDA tensors. The below diagram of different activation functions compares the original $$sign$$ function used in the binary network(yellow) to the piecewise ternary function (red).

![Activation Functions](./ActivationFunctions.png)


## Work To Be Completed

Based on the results from Torch several concerns are rasied related to the accuracy of the Top-1 and Top-5 test results surrounding the values of alexnet and its binary weighted version. Based on the published papers surrounding AlexNet, the binary weights should of decrased the accuracy in the tested range by approximately 10%-15%. In fact the opposite occured and the time needed to train one mini-batch increased to 2.2s from 0.8s. This increased in time and accuracy indicated that possible tensor math was calculated incorrectly.

> Note the code in question was never modified for use in the ternary network. That code came from the XNOR-Net model.

At the time of writing the same network architectures were being trained in TensorFlow and will be included in the next progress report. As such the next step will be to repeat the testing conducted over the last week on the TensorFlow framework. 

Additionally a more robust testing suite allowing more configurations of networks including different activation functions and values will need to be crerated in the coming weeks to continue development of the networks. TensorFlow will allow an increase in development speed and has a better and more robust testing suite that catch errors in numerical calculations.

After such changes have been completed several more tasks can move forward including:

- Running the network on more datasets. So far only imagenet an CIFAR have been used.
- Using a heuristic algorithm like simulated annealing or a genetic algorithm to choose the most suitable variables for the network including ternary activation levels, channel numbers, and possibly the number of levels in the activation function (1,3,5,etc..)
- Running the network on more challenging hardware - possibly a raspberry pi or NVIDIA TX2.
- Using live data from a camera and having the network update in time.