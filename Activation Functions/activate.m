close all
clear variables;

x = -10:0.001:10;
sigmoid = @(x)(2./(1+exp(-x))-1);



figure(1)
hold on
plot(x, sigmoid(x));
plot(x, sign(x));
plot(x, tanh(x));
plot(x, ternary(x));
title('Activation Functions')
xlabel('weighted sum')
ylabel('activation value')
legend('sigmoid', 'sign', 'tanh', 'ternary');
hold off

function y=ternary(x)
y = zeros(size(x));
for i=1:length(x)
    if ((x(i)) >= 2)
        y(i) = 1;
    else
        if ((x(i) <= -2))
            y(i) = -1;
        else
            y(i) = 0;
        end
    end
end

end