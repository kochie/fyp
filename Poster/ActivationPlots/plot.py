#!/bin/env python3

import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np


def sigmoid_array(x):
     return 2/(1 + np.exp(-x))-1


def main():
    x = np.linspace(-5, 5, 100)

    plt.plot(x, np.zeros(len(x)))
    plt.plot(x, np.tanh(x), label='tanh')
    plt.plot(x, sigmoid_array(x), label='sigmoid')
    plt.plot(x, 2*np.heaviside(x, 0)-1, label='binary step')
    plt.plot(x, np.piecewise(x, [x < -2, x >= 2], [-1, 1]), label='ternary step')

    plt.xlabel("Sum of Weights")
    plt.ylabel("Node Output")

    plt.title("Activation Functions")

    plt.legend()

    plt.show()


if __name__ == "__main__":
    main()
